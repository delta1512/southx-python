import endpoints as e
import southx as sx

'''
The following is an example script for the southxchange python wrapper

Please read the API documentation provided by southxchange:
https://www.southxchange.com/Home/Api

Parameters in the query_pub function are lists whereas the query_priv ones
are python dictionaries. You are responsible for providing the correct
parameters, this script will only communicate with the southxchange site
and not check the validity of provided parameters.

When using the query_priv function, make sure you copy `secret.py.skel` to a
new file called `secret.py` and change your credentials inside.

Check `endpoints.py` for a list of all possible API functions.

Queries will return None if an error occurred.
'''


# Query the public API and get the pice of GRC/BTC
result = sx.query_pub(e.PRICE, ['GRC', 'BTC'])
# Show everything provided
print(result)
# Only show the price of the highest buy order
print(result['Bid'])


# Query the private API and get all your balances
# No parameters are required, hence {}
result = sx.query_priv(e.BALANCES, {})
print(result)


# Cancel all orders for GRC/BTC
# Set the parameters for the API call (refer to the southxchange documentation)
params = {
    'listingCurrency' : 'GRC',
    'referenceCurrency' : 'BTC'
}
# Query the private API
sx.query_priv(e.CANCEL_MARKET_ORDERS, params)
# (result will always be None)
