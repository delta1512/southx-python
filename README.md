# Southxchange API wrapper for Python

This set of scripts act as a wrapper for the [Southxchange trading API](https://www.southxchange.com/Home/Api). All included API calls can be found in [endpoints.py](./endpoints.py).

To use this in your own program, simply clone the respoistory and edit main with wour code.

See [main.py](./main.py) for example code and further details.
