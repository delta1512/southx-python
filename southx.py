from time import time
import requests
import hashlib
import hmac
import json

import secret as s

OK = 200
# Basic header for GET and POST
HEADER_TEMPLATE = {'Content-Type' : 'application/json', 'Accept' : '*/*'}


def query_pub(url: str, payload: list):
    '''
    Queries the public southxchange API.
    Make sure all parameters are in order of how their URL is presented.
    e.g ...price/{listingCurrencyCode}/{referenceCurrencyCode}
        will require a list with ['GRC', 'BTC']
    '''
    r = requests.get(url.format(*payload))

    if r.status_code == OK:
        return r.json()
    else:
        return None



def query_priv(url: str, payload: dict, nonce=None):
    '''
    Queries the private southxchange API.
    Make sure you fill out your secret.py file with your API keys.
    All parameters are passed as a dictionary.
    The nonce, API key and signiature are all handled automatically.
    '''
    head = HEADER_TEMPLATE
    # If no nonce provided, use the current time
    if nonce is None:
        nonce = time()
    # Add the required parameters to the JSON body
    payload['nonce'] = nonce
    payload['key'] = s.API_KEY

    # Generate a HMAC signiature using a sha512 hash of the params
    sig = hmac.new(s.SECRET.encode(), msg=json.dumps(payload).encode(), digestmod=hashlib.sha512).digest()
    sig = hex(int.from_bytes(sig, 'big'))[2:] # Convert to hex
    head['Hash'] = sig # Include it in the header

    r = requests.post(url, data=json.dumps(payload), headers=head)

    if r.status_code == OK:
        return r.json()
    else:
        return None
