BASE_URL = 'https://www.southxchange.com/api/'


# Public endpoints
MARKETS = BASE_URL + 'markets'
MARKETSV2 = BASE_URL + 'v2/markets'
PRICE = BASE_URL + 'price/{}/{}' # {listingCurrencyCode}/{referenceCurrencyCode}
PRICES = BASE_URL + 'prices'
BOOK = BASE_URL + 'book/{}/{}' # {listingCurrencyCode}/{referenceCurrencyCode}
TRADES = BASE_URL + 'trades/{}/{} ' # {listingCurrencyCode}/{referenceCurrencyCode}
HISTORY = BASE_URL + 'history/{}/{}/{}/{}/{}' # {listingCurrencyCode}/{referenceCurrencyCode}/{start}/{end}/{periods}


# Private endpoints
PLACE_ORDER = BASE_URL + 'placeOrder'
CANCEL_ORDER = BASE_URL + 'cancelOrder'
CANCEL_MARKET_ORDERS = BASE_URL + 'cancelMarketOrders'
LIST_ORDERS = BASE_URL + 'listOrders'
GET_ADDRESS = BASE_URL + 'generatenewaddress'
WITHDRAW = BASE_URL + 'withdraw'
BALANCES = BASE_URL + 'listBalances'
TRANSACTIONS = BASE_URL + 'listTransactions '
